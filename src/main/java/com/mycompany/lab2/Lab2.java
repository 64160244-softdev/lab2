/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    private static char[][] board;
    private static char currentPlayer;

    public static void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }

    public static void initializeBoard() {
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
        printBoard();
    }

    public static void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    public static void inputMove() {
        System.out.println("It's player " + currentPlayer + "'s turn.");
        System.out.print("Please input row [1-3] and column [1-3] for your move [ex:1 1] : ");
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt() - 1;
        int col = sc.nextInt() - 1;
        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            board[row][col] = currentPlayer;
        } else {
            System.out.println("Invalid move Please try agian.");
            inputMove();
        }

    }

    public static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true;
            }
        }
    return false;
    }
    
    public static boolean checkCol(){
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }
    public static boolean checkDiagonal1(){
        if(board[0][0] == currentPlayer  && board[1][1] == currentPlayer && board[2][2] == currentPlayer ){
            return true;     
        }
        return false;
    }
    public static boolean checkDiagonal2(){
        if(board[0][2]== currentPlayer && board[1][1]== currentPlayer && board[2][0] == currentPlayer){
            return true;  
        }
        return false;
    }
    
    public static boolean hasWon(){
        if(checkRow() || checkCol() || checkDiagonal1() || checkDiagonal2()){
            return true;
        }
        return false;       
    }
    
    public static boolean isDraw(){
        for(int i = 0 ; i < 3 ; i++){
            for(int j = 0 ; j < 3 ; j++){
                if(board[i][j] != '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    public static void switchPlayer(){
        currentPlayer = currentPlayer == 'X'?'O':'X';
    }
    
    public static void chooseFirstPlayer(){
        Scanner sc = new Scanner(System.in);
        System.out.println("|---------------------|");
        System.out.println("| Choose who go first |");
        System.out.println("|    Press 1 is X     |");
        System.out.println("|    Press 2 is O     |");
        System.out.println("|---------------------|");
        System.out.print("Choose here : ");
        int firstPlayer = sc.nextInt();
        if(firstPlayer == 1){
            currentPlayer = 'X';
            System.out.println(currentPlayer+" go first");
        }else if(firstPlayer == 2){
            currentPlayer = 'O';
            System.out.println(currentPlayer+" go first");
        }else{
            System.out.println("Invalid choice Please try agian.");
            chooseFirstPlayer();
        }
        
    }
    
    public static  void play() {
        Scanner sc = new Scanner(System.in);
        boolean gameEnded = false;
        chooseFirstPlayer();
        initializeBoard();   
        while(!gameEnded){
           inputMove();
           printBoard();
           if(hasWon()){
               System.out.println("Congratulations! Player "+currentPlayer+" wins!");
               gameEnded = true;
           }else if(isDraw()){
                System.out.println("The result is Draw!");
                gameEnded = true;
           }else{
               switchPlayer();
           }      
        }
        System.out.print("Do you want to play again? (y/n) : ");
        String newGame = sc.next();
        if(newGame.equalsIgnoreCase("y")){
           play();
        }else if(newGame.equalsIgnoreCase("n")){
           System.out.println("Thank you for playing! Goodbye!");
        }   
    }

    
    public static void main(String[] args) {
        printWelcome();
        play();
    }
}
